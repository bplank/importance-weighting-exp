
import numpy as np
from scipy.stats.stats import pearsonr
import sys

file1=[]
for line in open(sys.argv[1]):
    file1.append([float(x) for x in line.strip().split()[1:]])

file2=[]
for line in open(sys.argv[2]):
    file2.append([float(x) for x in line.strip().split()[1:]])


m1=np.matrix(file1)
m2=np.matrix(file2)
#print m1,m2

for i in xrange(0,m1.shape[1]-1):
    a=m1[:,i] # get row
    b=m2[:,i]
    print pearsonr(a,b)[0]
