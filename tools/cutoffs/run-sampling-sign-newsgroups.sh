DOMAIN=newsgroups
ITERS=50
BASELINEPREDICTION=$IW_HOME/output/cpos/baseline/predictions/$DOMAIN.predicted
for ta in 0.946 0.947 0.948 0.949 0.950 0.951 0.952 0.953 0.954 0.955 0.956 0.957 0.958 0.959 0.960
do
    echo "-----SAMPLE " $ta
    for i in `seq 1 $ITERS`
    do
	echo "SAMPLE-ITER" $ta $i
	python $IW_HOME/tools/sample_baseline.py --ta $ta $BASELINEPREDICTION > sample.$DOMAIN
	$LOWLANDS_HOME/tools/signf_pos/run-signf.sh $IW_HOME/data/cpos/eval/gweb-$DOMAIN-test.conll $BASELINEPREDICTION sample.$DOMAIN
    done
done