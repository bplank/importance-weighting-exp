mkdir -p sign

for d in answers reviews emails weblogs newsgroups
do
    #grep "current ta" nohup.err.sampling-sign.$d | awk '{print $3}' > /tmp/a
    grep "current ta" nohup.err.sampling.$d| awk '{print $3}' > /tmp/a
    #grep "p-val" nohup.sampling-sign.$d  | awk '{print $5}' > /tmp/b
    grep p.val nohup.sampling.$d | awk '{print $3}'|sed 's/"//g' > /tmp/b
    grep "SAMPLE-ITER" nohup.sampling.$d > /tmp/c
    paste /tmp/a /tmp/b /tmp/c > sign/$d
done