#!/usr/bin/env python
import sys
import math
inp=map(str.strip,open(sys.argv[1]).readlines())
i=0
ans_test=[]
ans_dev=[]
param=[]
for line in inp:
    if "SCALE" in line:
        scale=line
    if "test=" in line:
        if "dev" in line:
            dev=True
        elif "test" in line:
            dev=False
        elif "test=None" in line:
            dev=False
    if "Accuracy" in line:
        acc=line.split()[6]

        if not dev:
            ans_test.append(acc)

        elif dev:
            ans_dev.append(acc)
            param.append(scale)


            


print "max:", max(ans_test), max(ans_dev)
i=0
for a,b,p in zip(ans_test,ans_dev,param):
    print i,a,b,p
    i+=1

#print >>sys.stderr, max(ans_test), max(ans_dev)

