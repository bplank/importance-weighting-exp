# -*- coding: utf-8 -*-
"""
compute accuracy for a CONLL style file where 
the penultimate column is the gold label and the last one the prediction

USAGE:
\t python accuracy_conll.py <file>

Created on Mon Aug 12 15:36:34 2013
@author: dirkhovy, bplank

CHANGES:
- add sentence level scores (May 2014)
"""

import sys
import argparse

parser = argparse.ArgumentParser(description="Compute tagging accuracy for CoNLL file")
parser.add_argument('infile', help="input file in CoNLL format")
parser.add_argument('-b', help="output sentence-leve accuracy scores",action="store_true", default=False)

args = parser.parse_args()
if not args.infile:
    print("specify a file")
    exit()

correct = 0.0
total = 0.0

correct_OOV=0.0
total_OOV=0.0

sentences_correct = 0.0
sentences_total = 0.0

warning = False
current_sentence_correct = True
in_sentence = True

#scores per sentence
correct_sent=0.0
total_sent=0.0

i=0
for line in open(args.infile, 'rU'):
    line = line.strip()
    if line:
        in_sentence = True
        total += 1.0
        total_sent +=1.0
        columns = line.split('\t')

        try:
            gold = columns[-2]
            prediction = columns[-1]
            word = columns[0]

            if "<OOV>-" in word:
                total_OOV+=1.0

            if gold == prediction:
                correct += 1.0
                correct_sent+=1.0
                if "<OOV>-" in word:
                    correct_OOV+=1.0
            else:
                current_sentence_correct = False

        except IndexError:
            sys.stderr.write('Segmentation error: "%s", skipping line\n' % (line))
            warning = True

    else:
        in_sentence = False
        sentences_total += 1.0
        sentences_correct += float(current_sentence_correct)
        current_sentence_correct = True
        if args.b:
            print >>sys.stderr, "sent %s TA: %.2f (%s out of %s words)" % (i,correct_sent/float(total_sent)*100, correct_sent, total_sent)
            correct_sent=0
            total_sent=0
        i+=1
print ""
# increment counts after last line
if in_sentence:
    sentences_total += 1.0
    sentences_correct += float(current_sentence_correct)
    
print "per-sentence accuracy: %.2f (%s out of %s sentences)" % (sentences_correct/sentences_total*100, sentences_correct, sentences_total)
print "per-word accuracy: %.2f (%s out of %s words)" % (correct/total*100, correct, total)
if total_OOV>0:
    print "per-word OOV_accuracy: %.2f (%s out of %s OOV word tokens)" % (correct_OOV/total_OOV*100, correct_OOV, total_OOV)

if warning:
    print "some lines were corrupted, accuracy might not be correct"
