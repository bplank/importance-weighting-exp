#!/bin/bash

#### PARAMETERS 
DATA=cpos

SUFFIX=$1
WEIGHTS=$2

#answers
DOMAIN=$3

#####

CLUSTER=$IW_HOME/data/wordclusters/alldomains.3-100.mst.k5-c1000-p1.out-paths
WIKTIONARY=$IW_HOME/data/extendedwiktionary/en.tags.li.onto


DIR=$IW_HOME/data/$DATA
TRAININ=$DIR/train/ontonotes-wsj-train.conll.vw.$SUFFIX
PASSES=5
LOWER=0
#QUANTILES=10
#SCALE=$4
#SCALE=1.0


TRAINUNLAB=data/unlabeled/ontonotes.unlabeled.txt.vocab

OUTPUT=output/w_$SUFFIX_`basename $WEIGHTS`
MODELDIR=$OUTPUT/models
mkdir -p $OUTPUT
mkdir -p $OUTPUT/train
TRAIN=$OUTPUT/train/ontonotes-wsj-train.conll.vw.weights
MODEL=$MODELDIR/`basename $TRAIN`.model.weighted
####

#create vw feature files
#./tools/create-vw-feats.sh -d $DATA -c $CLUSTER -l $LABELMAP

mkdir -p $MODELDIR

for SCALE in 1 #2 3 4 5 6 7 8 9 10 12 15 20 30 40 50 60 70 80 90 100
do
    for QUANTILES in 10 #1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 20 30 40 50 80 100
    do 
	echo "SCALE-QUANTILES:" $SCALE $QUANTILES 1>&2
	python tools/quantilize.py --scale $SCALE --lower $LOWER --quantiles $QUANTILES $WEIGHTS $TRAININ > $TRAIN
	cat $TRAIN | cut -d' ' -f2 | sort |uniq -c 1>&2
	python tools/rungsted/rungsted/labeler.py --train $TRAIN --final-model $MODEL --passes $PASSES --no-ada-grad

	for DOMAIN in answers reviews emails weblogs newsgroups
	do

            python tools/rungsted/rungsted/labeler.py --initial-model $MODEL --test $DIR/eval/gweb-$DOMAIN-dev.conll.vw.base  --predictions /tmp/$$.pred
            python tools/accuracy_conll.py /tmp/$$.pred
            rm /tmp/$$.pred
	done

	echo "wsj" #ontonotes-wsj-test.conll
	python tools/rungsted/rungsted/labeler.py --initial-model $MODEL --test $DIR/eval/ontonotes-wsj-dev.conll.vw.base  --predictions /tmp/$$.pred
	python tools/accuracy_conll.py /tmp/$$.pred
	rm /tmp/$$.pred
    done
done


