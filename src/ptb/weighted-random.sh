#!/bin/bash

#### PARAMETERS 
DATA=ptb
SUFFIX=base

WEIGHTS=$1

#DOMAIN=$2 # in diff to other before, eval on all domains

#####

CLUSTER=$IW_HOME/data/wordclusters/alldomains.3-100.mst.k5-c1000-p1.out-paths

DIR=$IW_HOME/data/$DATA
TRAININ=$DIR/train/ontonotes-wsj-train.conll.vw.$SUFFIX

PASSES=5
LOWER=0

OUTPUT=/tmp/$$-output/$DATA/wran_`basename $WEIGHTS`
MODELDIR=$OUTPUT/models
mkdir -p $OUTPUT
mkdir -p $OUTPUT/train
TRAIN=$OUTPUT/train/ontonotes-wsj-train.conll.vw.weights
MODEL=$MODELDIR/`basename $TRAIN`.model.weighted
####

mkdir -p $MODELDIR

for QUANTILES in 0
do
    for SCALE in 1
    do 
	echo "SCALE-QUANTILES:" $SCALE $QUANTILES 1>&2
	python tools/quantilize.py --scale $SCALE --lower $LOWER --quantiles $QUANTILES $WEIGHTS $TRAININ > $TRAIN
	cat $TRAIN | cut -d' ' -f2 | sort |uniq -c 1>&2
	python tools/rungsted/rungsted/labeler.py --train $TRAIN --final-model $MODEL --passes $PASSES --no-ada-grad --labels data/ptb/ptb.labels
	
	for DOMAIN in answers reviews emails weblogs newsgroups
	do
	    for f in test 
	    do
		python tools/rungsted/rungsted/labeler.py --initial-model $MODEL --test $DIR/eval/gweb-$DOMAIN-$f.conll.vw.base  --predictions /tmp/$$.pred
		python tools/accuracy_conll.py /tmp/$$.pred
		rm /tmp/$$.pred
	    done
	done
    done
done
rm -r $OUTPUT
