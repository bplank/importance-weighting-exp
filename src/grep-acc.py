import sys
import numpy as np
#### greps accuracies out from nohup files, assumes 6 files (5gweb+wsj)
results=[]
iterresults=[]
i=0
for line in sys.stdin:
    line=line.strip()
    if "per-word accuracy" in line:
        i+=1
        fields=line.split()
        #acc = float(fields[2])
        acc = float(fields[2])
        iterresults.append(acc)
        if i==6:
            i=0
            results.append(iterresults)
            iterresults=[]
        
print "iter answers reviews emails weblogs newsgroups wsj mean_nowsj mean"
for iteration,res in enumerate(results):
    results = np.array(res)
    mean=results.mean()
    mean_nowsj=results[:-1].mean()
    print "{} {} {} {}".format(iteration+1," ".join([str(x) for x in res]),mean_nowsj,mean)
            
        
