#!/usr/bin/env python
# extend wiktionary with pos-tags in training data
from collections import defaultdict
import codecs
import sys
trainfile="../cpos/train/ontonotes-wsj-train.conll"
wik="en.tags.li"
#read original wiktionary
dictionary = defaultdict(set)
for line in codecs.open(wik, encoding='utf-8'):
    en, word, tag, _ = line.strip().split("\t")
    dictionary[word].add(tag)

print >>sys.stderr, len(dictionary.keys())
for line in codecs.open(trainfile,encoding="utf-8"):
    line=line.strip()
    if line:
        word,tag=line.split("\t")
        dictionary[word].add(tag)
print >>sys.stderr, len(dictionary.keys())
for word in sorted(dictionary.keys()):
    tags = dictionary[word]
    for tag in tags:
        print u"en\t{0}\t{1}\t#{1}".format(word,tag)
