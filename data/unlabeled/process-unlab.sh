# convert to uft-8
for file in *.unlabeled.txt
do
    iconv -c -f ISO-8859-1 -t utf-8 $file > $file.utf8 
    rm $file
done

# wiktionary and wordclusters
for i in ontonotes.unlabeled.txt.utf8 gweb*utf8 
do
    echo $i
    python tag_with_wiktionary.py ../extendedwiktionary/en.tags.li.onto $i > $i.wik.v2
    python tag_with_wiktionary.py ../extendedwiktionary/en.tags.li.onto $i open > $i.wik.v2.open
    python tag_with_brown.py ../wordclusters/alldomains.3-100.mst.k5-c1000-p1.out-paths $i > $i.brown.v2
    python tag_with_brown.py ../wordclusters/alldomains.3-100.mst.k5-c1000-p1.out-paths $i ../extendedwiktionary/en.tags.li.onto > $i.brown.v2.open

done