import sys
import codecs
from collections import defaultdict
brown=defaultdict(list)
for line in codecs.open(sys.argv[1]):
    fields = line.strip().split("\t")
    word=fields[1]
    tag=fields[0]
    brown[word].append(tag)

onlyOpenclass=False
wiktionary=defaultdict(list)
if len(sys.argv)==4:
    onlyOpenclass=True
    # then assumes wiktionary as arg[3]

    for line in codecs.open(sys.argv[3]):
        fields = line.strip().split("\t")
        word=fields[1]
        tag=fields[2]
        wiktionary[word].append(tag)

def isopenclass(tags,word):
    if "VERB" in tags:
        return True
    if "NOUN" in tags:
        return True
    if "ADJ" in tags:
        return True
    if "ADV" in tags and word.endswith("ly"):
        return True
    else:
        return False
    
#print wiktionary
for line in codecs.open(sys.argv[2]):
    fields = line.strip().split()
    out=""
    for token in fields:
        if token in brown:
            tags=brown[token]
            orgtoken=token
            if len(tags)>1:
                token = "|".join(brown[token])
            else:
                token=tags[0] #v0:brown
            
            if onlyOpenclass:
                if orgtoken in wiktionary and not isopenclass(wiktionary[orgtoken],orgtoken):
                    token=orgtoken

        #v2: replace with NULL
        else:        
            token="NA"
        out+=token+" "
    print out.strip()


