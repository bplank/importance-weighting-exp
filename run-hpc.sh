### run.sh script but for cluster
mkdir -p results/cpos
### baseline: sensitivity to iterations
#~/submit.sh -j baseline-iters -c "src/cpos/baseline-iters.sh"
###

### importance weighting with text classifiers (informed)

#for DOMAIN in answers reviews weblogs newsgroups emails
for DOMAIN in weblogs
do
    for weights in data/weights/w.$DOMAIN.* 
    do
	~/submit.sh -j w.$DOMAIN.`basename $weights` -c "src/cpos/weighted-informed.sh $weights $DOMAIN"
    done
done

#afterwards inspect results:
#python tools/grep.py  w.answers.w.answers.bigram.tok.out2

### random weighting
# for sample in random_sample random_stdexp random_zipf3
# do
#     for weights in data/weights/$sample/random.*
#     do
# 	~/submit.sh -j $sample.`basename $weights` -c "src/cpos/weighted-random.sh $weights"
#     done
# done

# for sample in random_sample random_stdexp random_zipf3
# do
#     echo $sample
#     echo "answers reviews emails weblogs newsgroups" > results/cpos/$sample.results
#     for f in `ls -v runs/random/$sample/$sample.random.*.out`
#     do
# 	python tools/grep-random.py $f >> results/cpos/$sample.results
#     done
#     mkdir -p runs/random/$sample
# #    mv $sample.random.*.out runs/random/$sample
# #    rm $sample.random.*.out2
# done


## grep informed weighting
# for DOMAIN in answers reviews weblogs newsgroups emails
# do
#     touch results/cpos/$DOMAIN.informed
#     for w in w.$DOMAIN*out2 
#     do
# 	echo $w >> results/cpos/$DOMAIN.informed
# 	python tools/grep.py $w >> results/cpos/$DOMAIN.informed
#     done
# done